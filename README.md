# central-platform

#### 项目介绍
central-platform简称CP，基于Spring Cloud(Finchley.SR1) 、Spring Boot(2.0.1)、Spring Security jwt开发 基于layui前后分离的开发平台,其中包括Gateway网关、Oauth认证服务、User用户服务、 Eureka注册中心等多个服务, 为微服务开发所需配置管理、服务发现、断路器、智能路由、 微代理等,努力为企业级打造最全面的微服务开发解决方案; http://59.110.164.254:8066

#### 软件架构
软件架构说明


#### 安装教程
1. Docker 开启远程API
* 编辑该文件  vim /lib/systemd/system/docker.service

ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock

注意：也可以直接在ExecStart后面追加  
-H unix:///var/run/docker.sock -H 0.0.0.0:2375

端口可根据情况修改，不冲突就好；

让配置生效 重启Docker程序：

systemctl daemon-reload 

systemctl restart docker

用netstat查看

netstat -tnlp |grep 2375

查看端口是否打开：
lsof -i:2375

查看该应用（用curl进行测试)

curl 192.168.126.102:2375/info

*  开启2375端口，修改防火墙配置文件 
    
vi /etc/sysconfig/iptables

* 开启2375端口，修改防火墙配置文件   
  
vi /etc/sysconfig/iptables

-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT

-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT

-A INPUT -p tcp -m state --state NEW -m tcp --dport 3306 -j ACCEPT

-A INPUT -p tcp -m state --state NEW -m tcp --dport 8080 -j ACCEPT

-A INPUT -m state --state NEW -m tcp -p tcp --dport 2375 -j ACCEPT

-A INPUT -j REJECT --reject-with icmp-host-prohibited

-A INPUT -p tcp -m tcp --dport 2375 -m state --state NEW,ESTABLISHED -j ACCEPT

service iptables save

service iptables restart

* 从新查看防火墙状态

service iptables status

* 下面这个命令可以看到开放的端口

/sbin/iptables -L -n  

* 用netstat查看

netstat -ntpl |grep 2375

测试端口是否开放

* netstat -anp | grep 2375

* 使用docker Maven插件本地构建docker镜像并发布到远程服务器

<!--添加利用maven插件构建docker镜像的插件依赖-->
			<!-- 首先加入pom ${docker.image.prefix} ： 这个是你的dockerhub注册上面的名字 gitgeek 这个是我注册的 ${project.artifactId} ： 项目的名称 dockerDirectory ： dockerfile的文件路径 -->
			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.4.13</version>
				<configuration>
					<!--镜像名称-->
					<imageName>${project.name}:${project.version}</imageName>
					<dockerDirectory>src/main/docker</dockerDirectory>
					<!--安装了docker的主机，并且打开了api remote接口设置-->
					<dockerHost>http://192.168.126.102:2375</dockerHost>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.jar</include>
						</resource>
					</resources>
				</configuration>
			</plugin>
		</plugins>
		
*  快速安装docker和compose

由于国内网络环境原因，连接docker官方服务器异常的慢，这体现为在国内安装docker-engine和拉取镜像非常慢，甚至无法安装和拉取。

此脚本通过更换repository和registry mirror解决了此问题。

脚本特性

对于docker-engine的安装，脚本特性如下：

支持centos7,ubuntu,debian系统的安装

支持安装特定版本的docker

根据网络环境测试结果自动选取较快的repository,如国内使用mirrors.ustc.edu.cn，国外使用dockerproject.org

通过测试网络自动为国内设置hub-mirror.c.163.com registry mirror

对于compose的安装，脚本特性如下：

支持所有Linux系统的安装

支持安装特定版本的compose

由于compose托管在amazonaws，国内几乎无法访问，自动设置一个代理下载

使用方法

wget centos.bz/ezhttp.zip

unzip ezhttp.zip

cd ezhttp-master

./start.sh

之后会弹出一个菜单，输入2选择Some Useful Tools.

然后输入17选择安装docker，输入18选择安装compose

检查是否安装成功

docker-compose --version


*  方式一：Docker-compose部署Eureka高可用

以下解决方案参考网址：

https://blog.csdn.net/m0_37268363/article/details/81878625

https://blog.csdn.net/chengqiuming/article/details/80959500

用docke-compose搭建eureka集群,这里搭建三个eureka server节点,遵循两两注册原则
```ruby
application.yml
server:
  port: 1111
spring:
  application:
    name: eureka-server
  profiles: slave1
  zipkin:
    base-url: http://192.168.126.102:11008

eureka:
  server:
    enable-self-preservation: true    #关闭服务器自我保护，客户端心跳检测15分钟内错误达到80%服务会保护，导致别人还认为是好用的服务
    eviction-interval-timer-in-ms: 15000 #清理间隔（单位毫秒，默认是60*1000）5秒将客户端剔除的服务在服务注册列表中剔除#
  client:
    register-with-eureka: true  #false:不作为一个客户端注册到注册中心
    fetch-registry: true      #为true时，可以启动，但报异常：Cannot execute request on any known server
    serviceUrl:
      defaultZone: http://192.168.126.102:1112/eureka,http://192.168.126.102:1113/eureka
  instance:
    hostname: slave1        #Eureka实例的主机名
    prefer-ip-address: true
    instance-id: ${spring.application.name}:${spring.cloud.client.ip-address}:${spring.application.instance_id:${server.port}}
    lease-renewal-interval-in-seconds: 30    # 续约更新时间间隔（默认30秒）
    lease-expiration-duration-in-seconds: 90 # 续约到期时间（默认90秒）
logging:
  level:
    root: INFO
    com.central: INFO
    org.springframework.web: INFO
    org.hibernate: INFO
    org.hibernate.type.descriptor.sql.BasicBinder: TRACE
    org.hibernate.type.descriptor.sql.BasicExtractor: TRACE
---
server:
  port: 1112
spring:
  application:
    name: eureka-server
  profiles: slave2

  zipkin:
    base-url: http://192.168.126.102:11008
eureka:
  server:
    enable-self-preservation: true    #关闭服务器自我保护，客户端心跳检测15分钟内错误达到80%服务会保护，导致别人还认为是好用的服务
    eviction-interval-timer-in-ms: 15000 #5秒将客户端剔除的服务在服务注册列表中剔除
  client: 
    register-with-eureka: true  #false:不作为一个客户端注册到注册中心
    fetch-registry: true      #为true时，可以启动，但报异常：Cannot execute request on any known server
    serviceUrl:
      defaultZone: http://192.168.126.102:1111/eureka,http://192.168.126.102:1113/eureka
  instance:
    hostname: slave2        #Eureka实例的主机名
    prefer-ip-address: true
    instance-id: ${spring.application.name}:${spring.cloud.client.ip-address}:${spring.application.instance_id:${server.port}}
    lease-renewal-interval-in-seconds: 30    # 续约更新时间间隔（默认30秒）
    lease-expiration-duration-in-seconds: 90 # 续约到期时间（默认90秒）
logging:
  level:
    root: INFO
    com.central: INFO  
    org.springframework.web: INFO
    org.hibernate: INFO
    org.hibernate.type.descriptor.sql.BasicBinder: TRACE
    org.hibernate.type.descriptor.sql.BasicExtractor: TRACE
---
server:
  port: 1113
spring:
  application:
    name: eureka-server
  profiles: slave3
  zipkin:
    base-url:  http://192.168.126.102:11008
eureka:
  server:
    enable-self-preservation: true    #关闭服务器自我保护，客户端心跳检测15分钟内错误达到80%服务会保护，导致别人还认为是好用的服务
    eviction-interval-timer-in-ms: 15000 #5秒将客户端剔除的服务在服务注册列表中剔除
  client: 
    register-with-eureka: true  #false:不作为一个客户端注册到注册中心
    fetch-registry: true      #为true时，可以启动，但报异常：Cannot execute request on any known server
    serviceUrl:
      defaultZone: http://192.168.126.102:1111/eureka,http://192.168.126.102:1112/eureka
  instance:
    hostname: slave3        #Eureka实例的主机名
    prefer-ip-address: true
    instance-id: ${spring.application.name}:${spring.cloud.client.ip-address}:${spring.application.instance_id:${server.port}}
    lease-renewal-interval-in-seconds: 30    # 续约更新时间间隔（默认30秒）
    lease-expiration-duration-in-seconds: 90 # 续约到期时间（默认90秒）
logging:
  level:
    root: INFO
    com.central: INFO    
    org.springframework.web: INFO
    org.hibernate: INFO
    org.hibernate.type.descriptor.sql.BasicBinder: TRACE
    org.hibernate.type.descriptor.sql.BasicExtractor: TRACE


docker-compose.yml
version: "3"
services:
  slave1:  
    image: eureka-server:0.0.1-SNAPSHOT
    ports:
      - "1111:1111"
    environment:
      - spring.profiles.active=slave1
  slave2:    # 高可用eureka注册节点2
    image: eureka-server:0.0.1-SNAPSHOT   
    ports:
      - "1112:1112"
    environment:
      - spring.profiles.active=slave2
  slave3:    # 高可用eureka注册节点3
    image: eureka-server:0.0.1-SNAPSHOT
    ports:
      - "1113:1113"
    environment:
      - spring.profiles.active=slave3

```
      
运行 docker-compose up -d

自动生成eureka server 3个容器

他们之间相互维护一个心跳,若开启了Eureka Server自我保护机制,Eureka Server会将服务信息保存起来,让这些实例不会过期,尽可能的保护这些注册信息,当客户端向eureka集群注册时,发现有其他节点出现故障,会出现在不可用分片(unavailable-replicas,)但仍会向另一个正常工作的节点注册,等故障的两个节点排查好问题重新上线,正常的eureka会将客户端注册的请求转发给与它相连的其他注册中心,从而实现注册中心之间服务的同步
